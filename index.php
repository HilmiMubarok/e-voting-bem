<?php include 'config/koneksi.php'; cekStatus($_SESSION['user']); ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Voting BEM Fakultas</title>
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="assets/css/animate.css">
	<link rel="stylesheet" href="assets/css/fontawesome.css">
	<link rel="stylesheet" href="assets/css/style.css">
</head>
<body style="background-image: url('assets/img/bg.jpg'); background-repeat: no-repeat; background-size: cover;">
<!-- style="background-image: url('assets/img/bg.jpeg'); background-repeat: no-repeat; background-size: cover;" -->
	
	<?php 
		if (isset($_GET['page'])) {
			if ($_GET['page'] == "dashboarduser") {

				noSession('user', 'index.php');
				include 'dashboardUser.php';

			} elseif ($_GET['page'] == "logoutuser") {

				include 'logoutUser.php';

			} elseif ($_GET['page'] == "pilihcalon" && $_GET['id']) {

				noSession('user', 'index.php');
				include 'pilihCalon.php';
			}
		} else {
			
			hasSession('user', '?page=dashboarduser');
			include 'welcome.php';
		}
	?>	

	<?php
		if (isset($_POST['btnLogin'])) {
			loginPemilih($_POST);
		}
	?>

	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/sweetalert.min.js"></script>
	<script src="assets/js/popper.min.js"></script>
	<script src="assets/js/fontawesome-all.min.js"></script>
</body>
</html>