<nav class="navbar navbar-expand-lg navbar-dark bg-info shadow">
	<a href="?page=dashboarduser" class="navbar-brand">E-Voting</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapseNavbar">
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="collapse navbar-collapse" id="collapseNavbar">
		<ul class="navbar-nav mx-auto text-white">
			<!-- <li class="nav-item"><?= sudahMilih() ?> Orang Sudah Memilih</li> -->
		</ul>
		<a href="?page=logoutuser">
			<button class="btn btn-danger">
				<i class="fa fa-sign-out-alt"></i> Logout
			</button>
		</a>
	</div>
</nav>