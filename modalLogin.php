<div class="modal" id="modalLogin">
	  <div class="modal-dialog modal-dialog-centered animated bounceIn modal-lg modalLogin">
	    <div class="modal-content">
	      <div class="modal-header bg-primary text-white">
	        <h5 class="modal-title" id="exampleModalLongTitle">Login</h5>
	        <button type="button" class="text-white close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        <form method="post">
	        	<div class="form-group">
	        		<label>Username</label>
	        		<input type="text" name="username" class="form-control form-control-lg" autofocus>
	        	</div>
	        	<div class="form-group">
	        		<label>Password</label>
	        		<input type="password" name="password" class="form-control form-control-lg">
	        	</div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">
	        	<i class="fa fa-ban"></i> Batal
	        </button>
	        <button type="submit" name="btnLogin" class="btn btn-primary">
	        	<i class="fas fa-sign-in-alt"></i> Login
	        </button>
	        </form>
	      </div>
	    </div>
	  </div>
	</div>