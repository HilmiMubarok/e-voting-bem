<?php include 'navbarUser.php';?>

<div class="container mt-4">
	<div class="alert bg-danger text-white mt-3 mx-auto w-75" role="alert">
		<h4 class="alert-heading">PENTING !</h4>
		<p>
			Setelah Melakukan Pemilihan, Maka Otomatis Akun Anda
			Akan Terlogout Dan Tidak Bisa Dipakai Lagi
		</p>
		<hr>
		<p class="mb-0">
			<b>
				Pastikan Untuk Memilih Calon Yang Sesuai Dengan Keinginan Anda!
			</b>
		</p>
	</div>
	<div class="row">
		<?php $rows = tampilCalon(); foreach ($rows as $row): ?>

		<div class="col-md-4">

			<div class="card mb-4 shadow">
				<img class="card-img-top" src="assets/img/calon/<?= $row['foto'] ?>" width="20" alt="Card image cap" style="width: 100%; height: 250px;">
				<div class="card-body">
					<h3 class="card-title"><?= $row['nama'] ?></h3>
					<p class="card-text">
						<b>Visi</b> <br>
						<?= $row['visi'] ?>
					</p>
					<p class="card-text">
						<b>Misi</b> <br>
						<?= $row['misi'] ?>
					</p>
					<p class="card-text">
						<b>Program Kerja</b> <br>
						<?= $row['proker'] ?>
					</p>
					<a href="?page=pilihcalon&&id=<?= $row['id_calon'] ?>" class="btn btn-primary btn-block btn-lg rounded-0" onclick="return confirm('Yakin ?')">
						Pilih <?= $row['nama'] ?>
					</a>
				</div>
			</div>

		</div>
		<?php endforeach ?>
	</div>

</div>