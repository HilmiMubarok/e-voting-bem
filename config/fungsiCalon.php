<?php

function tampilCalon()
{
	global $conn; 

	$sql = "SELECT * FROM calon";
	$res = $conn->query($sql);
	$row = [];

	while ($rows = $res->fetch_assoc()) {
		$row[] = $rows;
	}
	
	return $row;
}

function tambahCalon($data, $foto)
{
	global $conn; 

	$nama         = antiInjek($data['nama']);
	$jurusan      = antiInjek($data['jurusan']);
	$foto         = uploadFotoCalon();
	$visi         = $data['visi'];
	$misi         = $data['misi'];
	$proker       = $data['proker'];

	if ($foto) {
		$sql = "INSERT INTO calon VALUES ('', '$nama', '$jurusan', '$foto', '$visi', '$misi', '$proker' )";
		if ($conn->query($sql)) {
			echo "
				<div class='alert bg-success animated fadeIn text-center text-white footer mb-0'>
					<h5>Data Telah Tersimpan</h5>
				</div>";
			echo "<meta http-equiv='refresh' content='1.5;url=?page=calon'>";
		} else {
			echo "
				<div class='alert bg-danger animated fadeIn text-center text-white footer mb-0'>
					<h5>Data Gagal Tersimpan</h5>
				</div>";
			echo "<meta http-equiv='refresh' content='1.5;url=?page=tambahcalon'>";
		}
	} else {
		echo "
			<div class='alert bg-danger animated fadeIn text-center text-white footer mb-0'>
				<h5>Foto Gagal Diupload</h5>
			</div>";
		echo "<meta http-equiv='refresh' content='1.5;url=?page=tambahcalon'>";
	}
}

function uploadFotoCalon()
{
	$namaFoto = $_FILES['foto']['name'];
	$tmpFoto  = $_FILES['foto']['tmp_name'];
	$pathTo   = "../assets/img/calon/".$namaFoto;

	move_uploaded_file($tmpFoto, $pathTo);

	return $namaFoto;
}

function cekJurusanCalon($jurusan)
{
	if ($jurusan == "TI") {
		return "Informatika";
	} elseif ($jurusan == "DKV") {
		return "Desain Komunikasi dan Visual";
	}
}

function editCalon($id)
{
	global $conn; 

	$sql = "SELECT * FROM calon WHERE id_calon = '$id' ";
	$res = $conn->query($sql);
	$row = $res->fetch_assoc();
	
	return $row;
}

function updateCalon($data, $id)
{
	global $conn; 

	$nama         = antiInjek($data['nama']);
	$jurusan      = antiInjek($data['jurusan']);
	$foto         = uploadFotoCalon();
	$visi         = $data['visi'];
	$misi         = $data['misi'];
	$proker       = $data['proker'];

	if ($foto) {
		$sql = "UPDATE calon SET nama = '$nama', jurusan = '$jurusan', foto = '$foto', visi = '$visi', misi = '$misi', proker = '$proker' WHERE id_calon = '$id' ";
		if ($conn->query($sql)) {
			echo "
				<div class='alert bg-success animated fadeIn text-center text-white footer mb-0'>
					<h5>Data Telah Terupdate</h5>
				</div>";
			echo "<meta http-equiv='refresh' content='1.5;url=?page=calon'>";
		} else {
			echo "
				<div class='alert bg-danger animated fadeIn text-center text-white footer mb-0'>
					<h5>Data Gagal Terupdate</h5>
				</div>";
			echo "<meta http-equiv='refresh' content='1.5;url=?page=calon'>";
		}
	} else {
		$sql = "UPDATE calon SET nama = '$nama', jurusan = '$jurusan', visi = '$visi', misi = '$misi', proker = '$proker' WHERE id_calon = '$id' ";
		if ($conn->query($sql)) {
			echo "
				<div class='alert bg-success animated fadeIn text-center text-white footer mb-0'>
					<h5>Data Telah Terupdate</h5>
				</div>";
			echo "<meta http-equiv='refresh' content='1.5;url=?page=calon'>";
		} else {
			echo "
				<div class='alert bg-danger animated fadeIn text-center text-white footer mb-0'>
					<h5>Data Gagal Terupdate</h5>
				</div>";
			echo "<meta http-equiv='refresh' content='1.5;url=?page=calon'>";
		}
	}
}

function hapusCalon($id)
{
	global $conn;

	$fotoCalon    = editCalon($id);
	$rowFotoCalon = "../assets/img/calon/".$fotoCalon['foto'];

	$sql = "DELETE FROM calon WHERE id_calon = '$id' ";
	if ($conn->query($sql)) {
		if (file_exists($rowFotoCalon)) {
			unlink($rowFotoCalon);
		}
		echo "
			<div class='alert bg-success animated fadeIn text-center text-white footer mb-0'>
				<h5>Data Telah Tehapus</h5>
			</div>";
		echo "<meta http-equiv='refresh' content='1.5;url=?page=calon'>";
	}
}

function jumlahCalon()
{
	global $conn;

	$sql = "SELECT * FROM calon";
	$res = $conn->query($sql);

	$num = $res->num_rows;
	return $num;
}