<?php

function loginAdmin($data)
{
	global $conn;
	
	$username = $data['username'];
	$password = $data['password'];
	$sql      = "SELECT * FROM admin WHERE username = '$username' ";
	$res      = $conn->query($sql);

	if ($res->num_rows === 1) {

		$row  = $res->fetch_assoc();
		$usr  = $row['username'];
		if ( password_verify($password, $row['password']) ) {

			$_SESSION['admin'] = "$usr";
			echo "
				<div class='alert bg-success animated fadeIn text-center text-white footer mb-0'>
					<h5>Login Sukses</h5>
				</div>";
			echo "<meta http-equiv='refresh' content='1.5;url=?page=dashboard'>";
		} else {
			echo "
				<div class='alert bg-danger animated fadeIn text-center text-white footer mb-0'>
					<h5>Login Gagal</h5>
				</div>";
			echo "<meta http-equiv='refresh' content='1.5;url=../admin'>";
		}
	} else {
		echo "
			<div class='alert bg-danger animated fadeIn text-center text-white footer mb-0'>
				<i class='fa fa-ban'></i> <h5 class='d-inline-block'>Username Tidak Terdaftar</h5>
			</div>";
		echo "<meta http-equiv='refresh' content='1.5;url=../admin'>";
	}
}