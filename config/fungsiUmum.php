<?php

function baseUrl($dir = "")
{
	return "http://localhost/voting/" .$dir;
}

function noSession($sess, $loc)
{
	if ( !isset($_SESSION["$sess"]) ) {
		header("location:".$loc);
	}
}

function hasSession($sess, $loc)
{
	if ( isset($_SESSION["$sess"]) ) {
		header("location:".$loc);
	}
}

function delSession($sess)
{
	if ( isset($_SESSION["$sess"]) ) {
		unset($_SESSION["$sess"]);
	}
}

function antiInjek($data)
{
	global $conn;
	return htmlspecialchars(stripcslashes(mysqli_real_escape_string($conn, $data)));
}