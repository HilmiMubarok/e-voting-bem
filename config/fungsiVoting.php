<?php

function sudahMilih()
{
	global $conn;

	$sql = "SELECT * FROM user WHERE status = 'Y' ";
	$res = $conn->query($sql);

	$num = $res->num_rows;

	return $num;
}

function belumMilih()
{
	global $conn;

	$sql = "SELECT * FROM user WHERE status = 'N' ";
	$res = $conn->query($sql);

	$num = $res->num_rows;

	return $num;
}

function pilihCalon($id)
{
	global $conn; 

	$pemilihSession       = tampilPemilihSession();
	$usernamePemilih      = $pemilihSession[0]['username'];
	$tampilCalon          = "SELECT * FROM calon WHERE id_calon = '$id' ";
	$resCalon             = $conn->query($tampilCalon);
	$rowCalon             = $resCalon->fetch_assoc();

	$pilihCalonUbahStatus = "UPDATE user SET calon_id = '$rowCalon[id_calon]', status = 'Y', expired = 'Y' WHERE username = '$usernamePemilih' ";
	
	if ($conn->query($pilihCalonUbahStatus)) {
		echo "
			<div class='alert bg-success animated fadeIn text-center text-white footer mb-0'>
				<h5>Sukses Anda Telah Melakukan Pemilihan</h5>
			</div>";
		echo "<meta http-equiv='refresh' content='1.5;url=?page=logoutuser'>";
	}
	
	return $rowCalon;
}

function lihatSuaraPerCalon($idCalon)
{
	global $conn;

	$sql = "SELECT * FROM user WHERE calon_id = '$idCalon' ";
	$res = $conn->query($sql);

	$num = $res->num_rows;

	return $num;
}

function presentase($idCalon)
{
	return round(lihatSuaraPerCalon($idCalon) / sudahMilih() * 100). "%" ;
}