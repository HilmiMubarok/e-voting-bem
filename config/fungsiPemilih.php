<?php

function cekStatus($username)
{
	global $conn; 

	$sql = "SELECT * FROM user WHERE username = '$username' AND status = 'Y' ";
	$res = $conn->query($sql);
	if ($res->num_rows > 0) {
		echo "sudah milih";
	}
	
	return 0;
}

function tampilPemilih()
{
	global $conn; 

	$sql = "SELECT * FROM user";
	$res = $conn->query($sql);
	$row = [];

	while ($rows = $res->fetch_assoc()) {
		$row[] = $rows;
	}
	
	return $row;
}

function tampilPemilihSession()
{
	global $conn; 

	$sql = "SELECT * FROM user WHERE username = '$_SESSION[user]' ";
	$res = $conn->query($sql);
	$row = [];

	while ($rows = $res->fetch_assoc()) {
		$row[] = $rows;
	}
	
	return $row;
}

function tambahPemilih($data)
{
	global $conn;

	$jumlahAkun = $data['jml'];

	for ($i=0; $i < $jumlahAkun ; $i++) { 
		$username = randomUsername(8);
		$password = randomPassword(14);

		$sql      = "INSERT INTO user VALUES ('' , '$username', '$password', 'N', '', 'N') ";

		if ($conn->query($sql)) {
			echo "
				<div class='alert bg-success animated fadeIn text-center text-white footer mb-0'>
					<h5>Data Telah Tersimpan</h5>
				</div>";
			echo "<meta http-equiv='refresh' content='1.5;url=?page=pemilih'>";
		}
	}
}

function randomUsername($length)
{
	$karakter= 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz123456789';
	$string = '';
		for ($i = 0; $i < $length; $i++) {
			$pos = rand(0, strlen($karakter)-1);
			$string .= $karakter{$pos};
		}
	return $string;
}

function randomPassword($length)
{
	$karakter= 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz123456789';
	$string = '';
		for ($i = 0; $i < $length; $i++) {
			$pos = rand(0, strlen($karakter)-1);
			$string .= $karakter{$pos};
		}
	return $string;
}

function loginPemilih($data)
{
	global $conn;

	$username = $data['username'];
	$password = $data['password'];

	$sql      = "SELECT * FROM user WHERE username = '$username' AND password = '$password' AND expired = 'N' ";
	$res      = $conn->query($sql);
	$row      = $res->fetch_assoc();
	$user     = $row['username'];

	if ($res->num_rows === 1) {
		$_SESSION['user'] = $user;

		echo "
			<div class='alert bg-success animated fadeIn text-center text-white footer mb-0'>
				<h5>Login Sukses</h5>
			</div>";
		echo "<meta http-equiv='refresh' content='1.5;url=?page=dashboarduser'>";
	} else {
		echo "
			<div class='alert bg-danger animated fadeIn text-center text-white footer mb-0'>
				<h5>Login Gagal</h5>
			</div>";
		echo "<meta http-equiv='refresh' content='1.5;url=index.php'>";
	}
}

function jumlahPemilih()
{
	global $conn;

	$sql = "SELECT * FROM user";
	$res = $conn->query($sql);

	$num = $res->num_rows;
	return $num;
}