<div class="container mt-5">
	<h1 class="text-center">
		E-Voting Ketua BEM Fakultas Komputer dan Desain <br>
		UNISS Kendal
	</h1>
	<div class="text-center">
		<button class="btn btn-primary btn-lg btnHomeLogin mt-5" data-toggle="modal" data-target="#modalLogin">
			<i class="fas fa-sign-in-alt"></i> LOGIN
		</button>
	</div>
</div>

<?php include 'modalLogin.php'; ?>