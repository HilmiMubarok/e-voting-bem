-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 24 Mei 2018 pada 07.42
-- Versi Server: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_voting`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `username` varchar(10) NOT NULL,
  `password` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id_admin`, `username`, `password`) VALUES
(1, 'admin', '$2y$10$oDDopX5tF1xfkus11llNAeQn6MUwuD0WQwdxB/5kL99B2B8MxNtZ2');

-- --------------------------------------------------------

--
-- Struktur dari tabel `calon`
--

CREATE TABLE `calon` (
  `id_calon` int(11) NOT NULL,
  `nama` text NOT NULL,
  `jurusan` varchar(20) NOT NULL,
  `foto` text NOT NULL,
  `visi` text NOT NULL,
  `misi` text NOT NULL,
  `proker` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `calon`
--

INSERT INTO `calon` (`id_calon`, `nama`, `jurusan`, `foto`, `visi`, `misi`, `proker`) VALUES
(8, 'Hilmi', 'TI', 'Muhammad Hilmi Mubarok.jpg', '<ol><li>nganu</li><li>ngene</li></ol>', '<ol><li>ngono wae</li></ol>', '<ol><li>raono</li></ol>'),
(9, 'gfgdgf', 'DKV', '527327.jpg', '<ol><li>sfd</li></ol>', '<ol><li>sdf</li></ol>', '<ol><li>sdf</li></ol>'),
(10, 'Mubarok', 'DKV', 'Screenshot (60).png', '<p>asdadasd</p>', '<p>asd</p>', '<p>asd</p>');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(10) NOT NULL,
  `password` text NOT NULL,
  `status` varchar(2) NOT NULL,
  `calon_id` int(11) DEFAULT NULL,
  `expired` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `username`, `password`, `status`, `calon_id`, `expired`) VALUES
(176, 'NmtrTPjM', 'FLHHcCEx53pGcV', 'Y', 9, 'Y'),
(177, 'mWfeKZF5', '9mltUyEg4W5Y5E', 'Y', 9, 'Y'),
(178, 'ZkYKFZIu', 'WanSdf4XI9XFir', 'Y', 8, 'Y'),
(179, 'QIjiEZtQ', 'LYVjOws9Qyrc5N', 'Y', 8, 'Y'),
(180, 'obPLY72c', 'SZ2OLx1PYl4rxw', 'Y', 9, 'Y'),
(181, 'lUsxFWAm', '3dAtJ6hGbb9K2M', 'Y', 8, 'Y'),
(182, 'K5B7az93', 'm2XuGeQupqPms4', 'Y', 9, 'Y'),
(183, 'aiEHYSDx', 'PCg5igz6c7zzfD', 'Y', 8, 'Y'),
(184, '1EyCsl9n', 'y6FE7BsCeS9uQw', 'Y', 9, 'Y'),
(185, 'd8m5S3Zp', 'BO8fuMJZFgR1Ga', 'Y', 8, 'Y'),
(186, 'OwZnTqHV', 'XgvSwufrXX4Qdz', 'Y', 9, 'Y'),
(187, 'DO7GiSIn', 'fq2aeHe6ly2Ibf', 'Y', 9, 'Y'),
(188, 'UeGXr8KM', 'WBtIIoZ6WSRx37', 'Y', 8, 'Y'),
(189, 'msNXyaNL', 'srCe4R9vdX69Br', 'Y', 8, 'Y'),
(190, 'yQOTd4Yh', 'LRGUR3a2lp94hH', 'Y', 9, 'Y'),
(191, '1m3LovBV', 'biuizrbT5a7iSb', 'Y', 8, 'Y'),
(192, 'T7gLM7HX', 'LxJkWuexUdeeLO', 'Y', 9, 'Y'),
(193, '8lfrRmYI', 'RgbPRuuOKS5yPm', 'Y', 8, 'Y'),
(194, 'BPYroN69', '57rcaKEkOF1V7Z', 'Y', 8, 'Y'),
(195, 'ZMqXkGBs', 'shDmodVytXDhyN', 'Y', 8, 'Y'),
(196, 'dsxXr8ve', 'kgaCFAgUqePQVi', 'Y', 8, 'Y'),
(197, 'upX4ojTU', 'CWPPzm4AhcgaHb', 'Y', 10, 'Y'),
(198, 'AxFVYUp9', 'SzMjbqHPBrZkwQ', 'N', 0, 'N'),
(199, 'ip5KfHLw', 'tAu7O5XBa8QzTu', 'N', 0, 'N'),
(200, 'IsVVNhfV', 'MnP2UzjdTf4RXd', 'N', 0, 'N'),
(201, 'P2PFDVHM', 'SSsIbxRuceeYya', 'N', 0, 'N'),
(202, 'l3eMUl76', '3WJM8giFz6KNK5', 'N', 0, 'N'),
(203, 'anNUSApf', 'QVvTSTOE17Pn53', 'N', 0, 'N'),
(204, 'jV7NaD4e', 'eFQ1BRA3sBkoxH', 'N', 0, 'N');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `calon`
--
ALTER TABLE `calon`
  ADD PRIMARY KEY (`id_calon`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `calon`
--
ALTER TABLE `calon`
  MODIFY `id_calon` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=205;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
