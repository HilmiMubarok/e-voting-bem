<?php include 'navbar.php'; $row = editCalon($_GET['id']) ?>

<div class="container-fluid mt-2">
	<h1 class="text-center">
		Form Edit Calon <?= $row['nama'] ?>
	</h1>
	<div class="card shadow mb-3 rounded-0">
		<div class="card-body">
			<form method="post" novalidate enctype="multipart/form-data">
				<div class="form-group">
					<label>Nama</label>
					<input type="text" name="nama" class="form-control" value="<?= $row['nama'] ?>">
				</div>
				<div class="form-group">
					<label>Jurusan</label>
					<select name="jurusan" class="form-control">
						<?php if ($row['jurusan'] == "TI"): ?>
							<option value="TI">Informatika</option>
							<option value="DKV">Desain Komunikasi dan Visual</option>
						<?php elseif ($row['jurusan'] == "DKV"): ?>
							<option value="DKV">Desain Komunikasi dan Visual</option>
							<option value="TI">Informatika</option>
						<?php endif ?>
					</select>
				</div>
				<div class="form-group">
					<label>Foto</label>
					<div class="input-group">
						<div class="custom-file">
							<input type="file" name="foto" class="custom-file-input">
							<label class="custom-file-label"><?= $row['foto'] ?></label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label>Visi</label>
					<textarea name="visi" class="form-control" id="areaVisi" required><?= $row['visi'] ?></textarea>
				</div>
				<div class="form-group">
					<label>Misi</label>
					<textarea name="misi" class="form-control" id="areaMisi" required><?= $row['misi'] ?></textarea>
				</div>
				<div class="form-group">
					<label>Program Kerja</label>
					<textarea name="proker" class="form-control" id="areaProker" required><?= $row['proker'] ?></textarea>
				</div>
				<button type="submit" name="btnEditCalon" class="btn btn-success text-white btn-lg mb-5">
					<i class="fa fa-save"></i> Update
				</button>
			</form>
		</div>
	</div>
</div>