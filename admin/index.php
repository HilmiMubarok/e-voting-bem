<?php include '../config/koneksi.php'; ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Admin Voting</title>
	<link rel="stylesheet" href="../assets/css/bootstrap.min.css">
	<link rel="stylesheet" href="../assets/css/animate.css">
	<link rel="stylesheet" href="../assets/css/fontawesome.css">
	<link rel="stylesheet" href="../assets/css/style.css">
	<link rel="stylesheet" href="../assets/css/dataTables.bootstrap4.min.css"/>
	<link rel="stylesheet" href="../assets/css/buttons.bootstrap4.min.css"/>	
	<link rel="stylesheet" href="../assets/css/responsive.dataTables.min.css"/>

</head>
<body style="background-image: url('../assets/img/bg.jpg'); background-repeat: no-repeat; background-size: cover;">
	
	<?php

		if (isset($_GET['page'])) {
			if ($_GET['page'] == "reg") {

				include 'register.php';

			} elseif ($_GET['page'] == "dashboard") {

				noSession('admin', '../admin');
				include 'dashboard.php';

			} elseif ($_GET['page'] == "calon") {

				noSession('admin', '../admin');
				include 'calon.php';

			} elseif ($_GET['page'] == "printpemilih") {

				noSession('admin', '../admin');
				include 'printPemilih.php';

			} elseif ($_GET['page'] == "tambahcalon") {

				noSession('admin', '../admin');
				include 'tambahCalon.php';

			} elseif ($_GET['page'] == "editcalon" && $_GET['id']) {

				noSession('admin', '../admin');
				include 'editCalon.php';

			} elseif ($_GET['page'] == "hapuscalon" && $_GET['id']) {

				noSession('admin', '../admin');
				hapuscalon($_GET['id']);

			} elseif ($_GET['page'] == "pemilih") {

				noSession('admin', '../admin');
				include 'pemilih.php';

			} elseif ($_GET['page'] == "hasil") {

				noSession('admin', '../admin');
				include 'hasil.php';

			} elseif ($_GET['page'] == "lihathasil" && "pemenang") {
				
				noSession('admin', '../admin');
				noSession('hasil', '../admin/?page=hasil');
				include 'lihatHasil.php';

			} elseif ($_GET['page'] == "logout") {

				include 'logout.php';
			}
		} else {

			hasSession('admin', '?page=dashboard');
			include 'login.php';
		}

		if (isset($_POST['btnLogin'])) {

			loginAdmin($_POST);

		} elseif (isset($_POST['btnTambahCalon'])) {

			tambahCalon($_POST, $_FILES);

		} elseif (isset($_POST['btnEditCalon'])) {

			updateCalon($_POST, $_GET['id']);

		} elseif (isset($_POST['btnTambahPemilih'])) {
			
			tambahPemilih($_POST);
			delSession('hasil');
			
		}

	?>

	
	<script src="../assets/js/jquery.min.js"></script>
	<script src="../assets/js/bootstrap.min.js"></script>
	<script src="../assets/js/popper.min.js"></script>
	<script src="../assets/js/fontawesome-all.min.js"></script>
	<script src="../assets/js/pdfmake.min.js"></script>
	<script src="../assets/js/vfs_fonts.js"></script>
	<script src="../assets/js/jquery.dataTables.min.js"></script>
	<script src="../assets/js/dataTables.bootstrap4.min.js"></script>
	<script src="../assets/js/buttons.html5.min.js"></script>
	<script src="../assets/js/dataTables.responsive.min.js"></script>
	<script src="../assets/js/ckeditor.js"></script>
	<script src="../assets/js/script.js"></script>
</body>
</html>