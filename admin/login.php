<div class="card col-md-6 offset-md-3 mt-5 shadow rounded-0">
  <div class="card-body">
    <h5 class="card-title text-center">Login</h5>
    <p class="card-text">
		<form method="post">
			<div class="form-group">
				<label>Username</label>
				<input type="text" name="username" class="form-control form-control-lg" autofocus required>
			</div>
			<div class="form-group">
				<label>Password</label>
				<input type="password" name="password" class="form-control form-control-lg" required>
			</div>
			<button type="submit" name="btnLogin" class="btn btn-block btn-primary btn-lg">
				<i class="fa fa-sign-in-alt"></i> Login
			</button>
		</form>    	
    </p>
  </div>
</div>