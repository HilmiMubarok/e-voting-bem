<?php include 'navbar.php'; ?>

<div class="container-fluid mt-2">
	<h1 class="text-center mb-4 none-print">
		Selamat Datang di Halaman Pemilih
	</h1>
	<div class="row">
		<div class="col-md-6">
			
			<div class="card shadow mb-3 rounded-0">
				<div class="card-body">
					<div class="card-title">
						<h3 class="text-center">Daftar Pemilih</h3>
					</div>
					<div class="card-text">

						<a href="?page=printpemilih" target="_blank">
							<button class="btn btn-danger">
								<i class="fa fa-print"></i> Print
							</button>
						</a>

						<div class="table-responsive">
							<table class="table table-bordered" id="dataTablePemilih">
								<thead>
									<tr>
										<th>No.</th>
										<th>Username</th>
										<th>Password</th>
										<th class="status">Status</th>
									</tr>
								</thead>
								<tbody>
<?php $rows = tampilPemilih(); $no = 1; foreach ($rows as $row): ?>
									<tr>
										<td><?= $no ?></td>
										<td><?= $row['username'] ?></td>
										<td><?= $row['password'] ?></td>
										<td class="status">
<?php 
	if ($row['status'] == "N") {
		echo "<span class='text-danger'>
					<i class='fa fa-ban'></i>
					Belum Memilih
				</span>";
	} else { 
		echo "<span class='text-success'>
					<i class='fa fa-check'></i>
					Sudah Memilih
				</span>"; 
	} 
?>
										</td>
									</tr>
<?php $no++; endforeach ?>
								</tbody>
							</table>
						</div>
						
					</div>
				</div>
			</div>

		</div>
		<div class="col-md-6 none-print">
			<div class="card shadow mb-3 rounded-0">
				<div class="card-body">
					<div class="card-title">
						<h1 class="text-center">
							<h3 class="text-center">Tambah Pemilih</h3>
						</h1>
					</div>
					<div class="card-text">

						<form method="post">
							<div class="form-group">
								<label>Jumlah Akun</label>
								<input type="number" name="jml" class="form-control" required>
							</div>
							<button type="submit" name="btnTambahPemilih" class="btn btn-primary">
								<i class="fa fa-plus-circle"></i> Buat Akun
							</button>
						</form>
						<div class="alert alert-info mt-3" role="alert">
							<h4 class="alert-heading">INFO!</h4>
							<p>
								Untuk keamanan privasi pemilih, maka username dan password pemilih akan terbuat dari data acak.
							</p>
							<hr>
							<p class="mb-0">
								<b>
									Silahkan print data username dan password dan serahkan ke pemilih secara langsung!
								</b>
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> <br>
</div>