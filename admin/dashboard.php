<?php include 'navbar.php'; ?>

<div class="container-fluid mt-2">
	<h1 class="text-center">
		Selamat Datang di Halaman Admin
	</h1>
	<br>
	<div class="row">
		<div class="col-md-3">
			<div class="card shadow mb-3 bg-primary text-white rounded-0">
				<div class="card-body">
					<div class="card-title">
						<h1 class="text-center">
							<i class="fa fa-user"></i>
						</h1>
					</div>
					<div class="card-text text-center">
						<h3><?= jumlahPemilih() ?> Pemilih</h3>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="card shadow mb-3 bg-warning text-white rounded-0">
				<div class="card-body">
					<div class="card-title">
						<h1 class="text-center">
							<i class="fa fa-user"></i>
						</h1>
					</div>
					<div class="card-text text-center">
						<h3><?= jumlahCalon() ?> Calon</h3>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="card shadow mb-3 bg-success text-white rounded-0">
				<div class="card-body">
					<div class="card-title">
						<h1 class="text-center">
							<i class="fa fa-check"></i>
						</h1>
					</div>
					<div class="card-text text-center">
						<h3><?= sudahMilih() ?> Sudah Memilih</h3>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<div class="card shadow mb-3 bg-danger text-white rounded-0">
				<div class="card-body">
					<div class="card-title">
						<h1 class="text-center">
							<i class="fa fa-ban"></i>
						</h1>
					</div>
					<div class="card-text text-center">
						<h3><?= belumMilih() ?> Belum Memilih</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>