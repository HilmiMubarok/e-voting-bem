<?php include 'navbar.php'; ?>

<div class="container-fluid mt-2">
	<h1 class="text-center mb-4 none-print">
		Hasil Perhitungan Suara
	</h1>
	<div class="row">
		<?php $rows = tampilCalon(); foreach ($rows as $row): ?>
		<div class="col-md-4">
			
			<div class="card mb-4 shadow">
			  <img class="card-img-top" src="../assets/img/calon/<?= $row['foto'] ?>" width="20" alt="Card image cap" style="width: 100%; height: 250px;">
			  <div class="card-body">
			    <h1 class="card-title text-center"><?= $row['nama'] ?></h1>
			    <h3 class="card-text text-danger">
			    	<div class="text-center">
			    		Jumlah Suara : <?= lihatSuaraPerCalon($row['id_calon']) ?> <br>
			    		Presentase : <?= presentase($row['id_calon']) ?>
			    	</div>
			    </h3>
			  </div>
			</div>

		</div>
		<?php endforeach ?>
	</div>
</div>