<?php include 'navbar.php'; ?>

<div class="container-fluid mt-2">
	<h1 class="text-center">
		Form Tambah Calon
	</h1>
	<div class="card shadow mb-3 rounded-0">
		<div class="card-body">
			<form method="post" novalidate enctype="multipart/form-data">
				<div class="form-group">
					<label>Nama</label>
					<input type="text" name="nama" class="form-control" required autofocus>
				</div>
				<div class="form-group">
					<label>Jurusan</label>
					<select name="jurusan" class="form-control">
						<option>-- Pilih Jurusan --</option>
						<option value="TI">Informatika</option>
						<option value="DKV">Desain Komunikasi dan Visual</option>
					</select>
				</div>
				<div class="form-group">
					<label>Foto</label>
					<div class="input-group">
						<div class="custom-file">
							<input type="file" name="foto" class="custom-file-input">
							<label class="custom-file-label">Upload File</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label>Visi</label>
					<textarea name="visi" class="form-control" id="areaVisi" required></textarea>
				</div>
				<div class="form-group">
					<label>Misi</label>
					<textarea name="misi" class="form-control" id="areaMisi" required></textarea>
				</div>
				<div class="form-group">
					<label>Program Kerja</label>
					<textarea name="proker" class="form-control" id="areaProker" required></textarea>
				</div>
				<button type="submit" name="btnTambahCalon" class="btn btn-primary btn-lg mb-5">
					<i class="fa fa-save"></i> Simpan
				</button>
			</form>
		</div>
	</div>
</div>