<?php include 'navbar.php'; ?>

<div class="container-fluid mt-2">
	<h1 class="text-center mb-4 none-print">
		Selamat Datang di Halaman Hasil
	</h1>
	<div class="text-center">
		<h1>
			<?= jumlahPemilih() ?> / <span class="text-danger"> <?= sudahMilih() ?> </span>
		</h1>
		<?php if (sudahMilih() == jumlahPemilih()): $_SESSION['hasil'] = "lihathasil"; ?>
			<a href="?page=lihathasil&&pemenang">
				<button class="btn btn-success btn-lg">
					Lihat Hasil
				</button>
			</a>
		<?php else: ?>
			<button class="btn btn-success btn-lg" disabled="disabled">
				Lihat Hasil
			</button>
		<?php endif ?>
	</div>
	<div class="alert alert-info mt-3 mx-auto w-50" role="alert">
		<h4 class="alert-heading">INFO!</h4>
		<p>
			Tombol hasil akan bisa di klik kalau jumlah yang sudah memilih sama dengan jumlah pemilih
		</p>
		<hr>
		<p class="mb-0">
			Pastikan pemilih sudah memilih semua!
		</p>
	</div>
</div>