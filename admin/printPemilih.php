<?php

$data = tampilPemilih();

require_once '../vendor/autoload.php';

$mpdf = new \Mpdf\Mpdf();
$html = 

'
<h1>Daftar Pemilih</h1>
<table border="1" cellpadding="10" cellspacing="5" style="width:100%">
	<tr>
		<th>Username</th>
		<th>Password</th>
	</tr>
';

foreach ($data as $row) {
	$html .= 
	'
	<tr>
		<td>'. $row["username"] .'</td>
		<td>'. $row["password"] .'</td>
	</tr>
	';
}

$html .= 
'
</table>
';

$mpdf->WriteHTML($html);
$mpdf->Output('dataPemilih.pdf', \Mpdf\Output\Destination::INLINE);