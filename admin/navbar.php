<nav class="navbar navbar-expand-sm navbar-fixed navbar-dark bg-info shadow">
	<a href="?page=dashboard" class="navbar-brand">E-Voting</a>
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapseNavbar">
		<span class="navbar-toggler-icon"></span>
	</button>

	<div class="collapse navbar-collapse" id="collapseNavbar">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item">
				<a href="?page=dashboard" class="nav-link text-white">Home</a>
			</li>
			<li class="nav-item">
				<a href="?page=calon" class="nav-link text-white">Calon</a>
			</li>
			<li class="nav-item">
				<a href="?page=pemilih" class="nav-link text-white">Pemilih</a>
			</li>
			<li class="nav-item">
				<a href="?page=hasil" class="nav-link text-white">Hasil</a>
			</li>
		</ul>
<!-- 		<span class="navbar-text text-white">
			Halo <?= $_SESSION['admin'] ?>
		</span> -->
		<a href="?page=logout">
			<button class="btn btn-danger">
				<i class="fa fa-sign-out-alt"></i> Logout
			</button>
		</a>
	</div>
</nav>