<?php include 'navbar.php'; ?>

<div class="container-fluid mt-2">
	<h1 class="text-center">
		Selamat Datang di Halaman Calon
	</h1>
	<a href="?page=tambahcalon">
		<button class="btn btn-primary mt-2 mb-2">
			<i class="fa fa-plus"></i> Tambah Calon
		</button>
	</a>
	<div class="card shadow mb-3 rounded-0">
		<div class="card-body">
			
			<div class="table-responsive">
				<table class="table table-bordered" id="dataTable">
					<thead>
						<tr>
							<th>No.</th>
							<th>Nama</th>
							<th>Foto</th>
							<th>Jurusan</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
<?php $rows = tampilCalon(); $no = 1; foreach ($rows as $row): ?>
						<tr>
							<td><?= $no ?></td>
							<td><?= $row['nama'] ?></td>
							<td>
								<img src="../assets/img/calon/<?= $row['foto'] ?>" width="50">
							</td>
							<td><?= cekJurusanCalon($row['jurusan']) ?></td>
							<td>
								<a href="?page=editcalon&&id=<?= $row['id_calon'] ?>" style="text-decoration: none;">
									<button class="btn btn-warning text-white" title="Edit">
										<i class="fa fa-edit"></i>
									</button>
								</a>
								<a href="?page=hapuscalon&&id=<?= $row['id_calon'] ?>" onclick="return confirm('Yakin Ingin Hapus?')">
									<button class="btn btn-danger text-white" title="Edit">
										<i class="fa fa-trash"></i>
									</button>
								</a>
							</td>
						</tr>
<?php $no++; endforeach ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>